#!/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QMainWindow, QWidget, QPushButton, QLabel, QLineEdit, QTableWidget, QAction, QHBoxLayout, QVBoxLayout, QScrollArea, QMessageBox
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QPainter, QBrush, QPen, QPalette, QColor
from PyQt5.QtCore import Qt, QSize
import copy

class UI(QMainWindow):
	WINDOW_TITLE = 'Расчёт определителя квадратной матрицы'

	WINDOW_WIDTH = 900
	WINDOW_HEIGHT = 680

	N_LABEL = 'Размерность:'
	ACCEPT_N_BTN_LABEL = 'Применить'
	RES_BTN_LABEL = 'Посчитать!'

	RES_MASK = 'Результат = {}'

	def __init__(self):
		super().__init__()

		self.result = 0
		self.result_label = QLabel(self)

		self.n = 3
		self.n_field = QLineEdit(self)
		self.table = QTableWidget(self)

		self.scroll_area = QScrollArea(self)
		self.scroll_content = QWidget(self.scroll_area)
		self.scroll_layout = QVBoxLayout(self)

		self.color = 1

		self.initUI()

	def initUI(self):
		self.setGeometry(200, 200, self.WINDOW_WIDTH, self.WINDOW_HEIGHT)
		self.setWindowTitle(self.WINDOW_TITLE)

		widget = QWidget(self)
		self.setCentralWidget(widget)

		# Добавление меню
		menubar = self.menuBar()
		windowmenu = menubar.addMenu('Окно')
		infomenu = menubar.addMenu('Сведения')

		theme_action = QAction('Переключить тему', self)
		theme_action.setShortcut('Ctrl+T')
		windowmenu.addAction(theme_action)

		author_action = QAction('Об Авторе', self)
		author_action.setShortcut('Ctrl+A')
		infomenu.addAction(author_action)

		# Назначение действия для переключения темы приложения
		theme_action.triggered.connect(self.toggle_color)

		# Назначение действия для показа информации об авторе
		author_action.triggered.connect(self.author_info)

		# Компоновка лэйаутов
		app_area = QHBoxLayout()
		
		left_area = QVBoxLayout()
		right_area = QVBoxLayout()

		n_area = QHBoxLayout()

		# Кнопка применения размерности
		accept_n_btn = QPushButton(self.ACCEPT_N_BTN_LABEL)
		accept_n_btn.clicked.connect(self.update_n)

		n_area.addWidget(QLabel(self.N_LABEL))
		n_area.addWidget(self.n_field)
		n_area.addWidget(accept_n_btn)

		self.scroll_area.setMinimumWidth(300)
		self.scroll_area.setMaximumWidth(400)
		self.scroll_area.setWidgetResizable(True)
		self.scroll_content.setLayout(self.scroll_layout)

		self.scroll_area.setWidget(self.scroll_content)


		right_area.addWidget(self.scroll_area)
		right_area.addWidget(self.result_label)

		# Кнопка получения результата
		result_btn = QPushButton(self.RES_BTN_LABEL)
		result_btn.clicked.connect(self.get_res)
		left_area.addLayout(n_area)
		self.update_table() # Обновление состояния таблицы
		left_area.addWidget(self.table)
		left_area.addWidget(result_btn)

		app_area.addLayout(left_area)
		app_area.addLayout(right_area)

		widget.setLayout(app_area)

		self.toggle_color() # Применение стандартной темы
		self.show() # Показ окна

	# Обновление состояния таблицы
	def update_table(self):
		self.table.setColumnCount(self.n)
		self.table.setRowCount(self.n)

	# Обновление размерности в пределпх от 2 до 50 
	def update_n(self):
		try:
			val = int(self.n_field.text())
			if val > 1 and val <= 50:
				self.n = val
				self.update_table()
		except Exception as e:
			return

	# Получение определителя введённой матрицы
	def get_res(self):
		m = self.table_2_matrix()
		if m is not None:
			self.log_clear()
			self.result = self.det_gauss(m)
			self.result_label.setText(self.RES_MASK.format(self.result))


	# Получение данных из таблицы в виде матрицы
	def table_2_matrix(self):
		self.log_clear()
		matrix = []
		for i in range(self.n):
			s = []
			for j in range(self.n):
				try:
					s.append(int(self.table.item(j, i).text()))
				except Exception as e:
					self.log_str('Недопустимое значение для ячейки ({}, {})'.format(j + 1, i + 1))
					return None
			matrix.append(s)
		return matrix

	# Получение минора матрицы (deprecated)
	def minor(self, A, i, j ):
		M = copy.deepcopy(A)  # копирование!
		del M[i]
		for i in range(len(A[0])-1):
			del M[i] [j]
		return M

	# Получения определителя матрицы методом разложения (Рекурсивная функция) (deprecated)
	def det(self, A ):
		m = len( A )
		n = len( A[0] )
		if m != n:
			return None
		if n == 1:
			return A[0][0]
		signum = 1
		determinant = 0
		# разложение по первой строке
		for j in range( n ):
			determinant += A[0][j]*signum*self.det( self.minor( A, 0, j ) ) 
			signum *= -1
		return determinant

	# Получение определителя матрицы методом гаусса (С ведением лога выполнения программы)
	def det_gauss(self, A):
		f = 0
		deter = 1
		self.log_str('Исходная матрица:')
		self.log_matrix(A)
		self.log_str()
		self.log_str('Начало преобразования матрицы к треугольной:')
		for i in range(self.n):
			if A[i][i] == 0:
				for k in range(i+1, self.n):
					if A[k][i] != 0:
						self.log_str('Элемент ({}, {}) = 0'.format(i+1, i+1))
						self.log_str('Элемент ({}, {}) != 0\nЗначит меняем столбцы {} и {} местами.\nОпределитель при этом меняет знак на противоположный'.format(i+1, k+1, i+1, k+1))
						for j in range(self.n):
							temp = A[i][j]
							A[i][j] = A[k][j]
							A[k][j] = temp
						deter *= -1
						break
				self.log_str('Текущая матрица:')
				self.log_matrix(A)

			for t in range(i+1, self.n):
				f = A[t][i] / A[i][i]
				if abs(f) < 10**-9: continue
				self.log_str('Из столбца {0} вычитаем столбец {1} умноженный на {2:.2f}'.format(t+1, i+1, f))
				for j in range(i, self.n):
					A[t][j] -= A[i][j] * f

				self.log_str('Текущая матрица:')
				self.log_matrix(A)

		self.log_str('Приведёная матрица:')
		self.log_matrix(A)
		self.log_str()

		for i in range(self.n):
			deter *= A[i][i]

		return round(deter)

	# Логирование строки
	def log_str(self, l_str = None):
		if l_str is not None: log = '\n{}'.format(l_str)
		else: log = '\n'
		self.scroll_layout.addWidget(QLabel(log))

	# Логирование матрицы
	def log_matrix(self, matrix):
		for i in range(self.n):
			s_str = ''
			for j in range(self.n):
				s_str += '{0:.2f} '.format(matrix[j][i])
			self.log_str(s_str)

	# Очистка лога
	def log_clear(self):
		self.scroll_layout = QVBoxLayout(self)
		self.scroll_content = QWidget(self.scroll_area)
		self.scroll_content.setLayout(self.scroll_layout)
		self.scroll_area.setWidget(self.scroll_content)

	# Переключение темы оформления
	def toggle_color(self):
		palette = QPalette()
		if self.color == 0:
			palette.setColor(QPalette.Window, QColor(53, 53, 53))
			palette.setColor(QPalette.WindowText, Qt.white)
			palette.setColor(QPalette.Base, QColor(25, 25, 25))
			palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
			palette.setColor(QPalette.ToolTipBase, Qt.white)
			palette.setColor(QPalette.ToolTipText, Qt.white)
			palette.setColor(QPalette.Text, Qt.white)
			palette.setColor(QPalette.Button, QColor(53, 53, 53))
			palette.setColor(QPalette.ButtonText, Qt.white)
			palette.setColor(QPalette.BrightText, Qt.red)
			palette.setColor(QPalette.Link, QColor(235, 101, 54))
			palette.setColor(QPalette.Highlight, QColor(235, 101, 54))
			palette.setColor(QPalette.HighlightedText, Qt.black)
			self.setPalette(palette)
			self.color = 1
		elif self.color == 1:
			palette.setColor(QPalette.Window, Qt.white)
			palette.setColor(QPalette.WindowText, Qt.black)
			palette.setColor(QPalette.Base, QColor(240, 240, 240))
			palette.setColor(QPalette.AlternateBase, Qt.white)
			palette.setColor(QPalette.ToolTipBase, Qt.white)
			palette.setColor(QPalette.ToolTipText, Qt.white)
			palette.setColor(QPalette.Text, Qt.black)
			palette.setColor(QPalette.Button, Qt.white)
			palette.setColor(QPalette.ButtonText, Qt.black)
			palette.setColor(QPalette.BrightText, Qt.red)
			palette.setColor(QPalette.Link, QColor(66, 155, 248))
			palette.setColor(QPalette.Highlight, QColor(66, 155, 248))
			palette.setColor(QPalette.HighlightedText, Qt.black)
			self.setPalette(palette)
			self.color = 0

	def author_info(self):
		QMessageBox().information(self,
				"Об авторе", 
				"Приложение разработано студентом Нижегородского Губернского Колледжа группы 31П:\n\t{}\nСайт разработчика: {}\n".format(
					'Смертиным Дмитрием Анатольевичем (NaggaDIM)',
					'http://naggadim.ru'
			))