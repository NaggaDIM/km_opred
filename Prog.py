#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
from PyQt5.QtWidgets import QApplication
from UI import UI


if __name__ == '__main__':
	app = QApplication(sys.argv)
	app.setStyle("Fusion")
	ui = UI()
	sys.exit(app.exec_())
